import { MockDaoMock } from '../MockDb/MockDao.mock';
import {globalInfoLogger, NameCallerArgsReturnLogDaosInfoLevel} from '@shared';
import {IDataDao} from './data.dao';

export class DataDao extends MockDaoMock implements IDataDao {

    @NameCallerArgsReturnLogDaosInfoLevel('User')
    public async post(): Promise<any> {
        try {
            const db = await super.openDb();
            return db.users;
        } catch (err) {
            globalInfoLogger.error(err);
            throw err;
        }
    }
}
