import {NameCallerArgsReturnLogDaosInfoLevel, SequelizeConnection} from '@shared';
import {Data, IData} from '@entities';

export interface IDataDao {
    post: (datas: IData[]) => Promise<any>;
}

export class DataDao implements IDataDao {
    private dataRepository = SequelizeConnection.getInstance().getRepository(Data);

    @NameCallerArgsReturnLogDaosInfoLevel('Data')
    public async post(datas: IData[]): Promise<any> {
        return this.dataRepository.bulkCreate(datas);
    }
}
