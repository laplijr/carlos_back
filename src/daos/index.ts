const usingMockDb = (process.env.USE_MOCK_DB || '').toLowerCase();
let dataDaoPath = './Data/data.dao';

if (usingMockDb !== 'false') {
    dataDaoPath += '.mock';
}

// tslint:disable:no-var-requires
export const { DataDao } = require(dataDaoPath);
