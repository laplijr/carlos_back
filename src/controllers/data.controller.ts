import {
    ApiOperationPost,
    ApiPath,
} from 'swagger-express-ts';
import {controller, httpPost, interfaces} from 'inversify-express-utils';
import * as express from 'express';
import {BAD_REQUEST, OK} from 'http-status-codes';
import {globalInfoLogger, NameCallerArgsReturnLogControllersInfoLevel} from '@shared';
import { injectable } from 'inversify';
import {DataService, IDataService} from '../services/data.service';
import Busboy from 'busboy';
import csv from 'csv-parser';
import {CsvService, ICsvService} from '../services/csv.service';

interface IDataController {
    post: (
        request: express.Request,
        response: express.Response,
        next: express.NextFunction,
    ) => Promise<any>;
}

@ApiPath({
    path: '/datas',
    name: 'Data',
})
@controller(
    '/datas',
)
@injectable()
export class DataController implements interfaces.Controller, IDataController {

    private dataService: IDataService = new DataService();
    private csvService: ICsvService = new CsvService();

    public static TARGET_NAME = 'DataController';

    @NameCallerArgsReturnLogControllersInfoLevel('Data')
    @ApiOperationPost({
        description: 'Post data',
        summary: 'Get all users',
        parameters: {
          body: {
              type: 'csv',
          },
        },
        responses: {
            200: {
                description: 'Success',
                model: 'DataDTO',
            },
            400: {
                description: 'Bad request',
            },
        },
    })
    @httpPost('')
    public async post(
        request: express.Request,
        response: express.Response,
        next: express.NextFunction,
    ): Promise<any> {
        try {
            const busboy = new Busboy({ headers: request.headers });
            busboy.on('file',
                (fieldname: string, file: any, filename: string, encoding: string, mimetype: string) => {
                console.log(mimetype);
                if (mimetype !== 'text/csv' && mimetype !== 'application/vnd.ms-excel') {
                    return response.status(BAD_REQUEST).json({
                       error: 'File should be csv',
                    });
                }
                file.on('data', (data: any) => {
                    console.log('File [' + filename + '] got ' + data.length + ' bytes');
                });
                file.on('end', () => {
                    console.log('File[' + filename + '] finished');
                });
                this.csvService.getDatasFromFile(filename, file).then((datas) => {
                    this.dataService.post(datas);
                });
            }).on('finish', () => {
                response.status(OK).json({ message: 'Csv inserted in database' });
            });
            request.pipe(busboy);
        } catch (err) {
            globalInfoLogger.error(err.message, err);
            return response.status(BAD_REQUEST).json({
                error: err.message,
            });
        }
    }
}
