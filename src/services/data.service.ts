import {IData} from '@entities';
import {DataDao} from '@daos';
import {NameCallerArgsReturnLogServicesInfoLevel} from '@shared';
import {IDataDao} from '../daos/Data/data.dao';

/**
 * Interface for classes that represent an user service
 * @interface
 */
export interface IDataService {
    post: (datas: IData[]) => Promise<any>;
}

/**
 * Creates a new DataService
 * @classdesc Service called to get informations about usersss
 *
 * @class
 * @implements {IDataService}
 */
export class DataService implements IDataService {

    private dataDao: IDataDao = new DataDao();

    /**
     * post data
     * @returns {Promise<any>}
     */
    @NameCallerArgsReturnLogServicesInfoLevel('Data')
    public post(datas: IData[]): Promise<any> {
        return this.dataDao.post(datas);
    }
}
