import {IData} from '@entities';
import * as fs from 'fs';
import csv from 'csv-parser';
import ErrnoException = NodeJS.ErrnoException;

export interface ICsvService {
    getDatasFromFile: (filename: string, file: any) => Promise<any>;
}

export class CsvService implements ICsvService {

    public async getDatasFromFile(filename: string, file: any): Promise<any> {
        return new Promise((resolve, reject) => {
            const datas: IData[] = [];
            const writeStream = fs.createWriteStream(filename);
            file.pipe(writeStream);
            file
                .pipe(csv({separator: ';'}))
                .on('data', (data: IData) => {
                    datas.push(data);
                })
                .on('error', (err) => {
                    reject(err);
                })
                .on('end', () => {
                    if (process.env.NODE_ENV !== 'testing' && process.env.NODE_ENV !== undefined) {
                        fs.unlink(filename, (err: ErrnoException | null) => {
                            if (err) {
                                throw err;
                            }
                            console.log('File ' + filename + ' deleted!');
                        });
                    }
                    resolve(datas);
                });
        });
    }
}
