import {v4String} from 'uuid/interfaces';
import {
    Table,
    Column,
    Model,
    PrimaryKey,
    Default,
    AllowNull,
} from 'sequelize-typescript';
import {DataTypes} from 'sequelize';
import {ApiModel, ApiModelProperty} from 'swagger-express-ts';
import {Path} from 'typescript-rest';

export interface IData {
    id: v4String;
    name: string;
    value: string;
}

@ApiModel({
    description: 'Data Model',
    name: 'Data',
})
@Path('Data')
@Table({tableName: 'datas', paranoid: true})
/**
 * Creates a new Data
 * @classdesc Database datas
 * @class
 * @public
 * @implements {IData}
 * @hideconstructor
 */
export class Data extends Model<Data> implements IData {

    @ApiModelProperty({
        description: 'Id of a Data',
        required: true,
        type: 'v4String',
        example: ['75442486-0878-440c-9db1-a7006c25a39f'],
    })
    @PrimaryKey
    @Default(DataTypes.UUIDV4)
    @Column(DataTypes.UUID)
    /**
     * Id of the data
     * @member Data#id
     * @public
     * @type {v4String}
     */
    public id!: v4String;

    @ApiModelProperty({
        description: 'name of the data',
        required: true,
        example: ['Ronan', 'Alexis', 'Louka', 'Rémi'],
    })
    @AllowNull(false)
    @Column(DataTypes.STRING)
    /**
     * name of the data
     * @member Data#name
     * @public
     * @type {string}
     */
    public name!: string;

    @ApiModelProperty({
        description: 'value of the data',
        required: true,
        example: ['Laplaud', 'Leroy', 'Houlgatte--Bustamante', 'Castel'],
    })
    @AllowNull(false)
    @Column(DataTypes.STRING)
    /**
     * LastName of the user
     * @member User#lastName
     * @public
     * @type {string}
     */
    public value!: string;
}
