import {v4String} from 'uuid/interfaces';
import {ApiModel, ApiModelProperty} from 'swagger-express-ts';
import {Path} from 'typescript-rest';
import {IData} from './data.entity';

/**
 * Creates a new DataDTO
 * @classdesc Dto purchase
 *
 * @class
 * @implements {IUser}
 */
@ApiModel({
    description: 'Data DTO Model',
    name: 'DataDTO',
})
@Path('DataDTO')
export class DataDTO implements IData {

    @ApiModelProperty({
        description: 'Id of a Data',
        required: true,
        type: 'v4String',
        example: ['75442486-0878-440c-9db1-a7006c25a39f'],
    })
    public id: v4String;

    @ApiModelProperty({
        description: 'name of the data',
        required: true,
        example: ['Ronan', 'Alexis', 'Louka', 'Rémi'],
    })
    public name: string;

    @ApiModelProperty({
        description: 'value of the data',
        required: true,
        example: ['Laplaud', 'Leroy', 'Houlgatte--Bustamante', 'Castel'],
    })
    public value: string;

    constructor(name: string, id: v4String, value: string) {
        this.name = name;
        this.id = id;
        this.value = value;
    }
}
