import {Container} from 'inversify';
import {interfaces, TYPE} from 'inversify-express-utils';
import {DataController} from '../controllers/data.controller';

const container = new Container();
container.bind<interfaces.Controller>(TYPE.Controller)
    .to(DataController).inSingletonScope().whenTargetNamed(DataController.TARGET_NAME);

export default container;
