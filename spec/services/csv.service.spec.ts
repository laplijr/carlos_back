import {CsvService} from '../../src/services/csv.service';
import * as fs from 'fs';

describe('get array of datas from csv', () => {
    it('should return an array of data', () => {
        const writer = fs.createWriteStream('./spec/resources/test.csv');
        writer.write('id;name;value\n' +
            'b24f218f-e6f4-4ba7-a42c-4191c1003ede;carlos;ghosn\n' +
            '5db8a426-4e03-4843-acee-c15ebef5807e;jean;michel\n' +
            '1ca81ced-c991-44e0-97cc-763ef6f5e22f;paul;jack\n' +
            'e3544686-118e-492b-8bce-914429dd316f;pierre;patrick');
        const reader = fs.createReadStream('./spec/resources/test.csv');
        new CsvService().getDatasFromFile('./spec/resources/test.csv', reader).then((datas) => {
            expect(datas.length).toEqual(4);
        });
    });
});
