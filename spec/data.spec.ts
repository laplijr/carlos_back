import supertest, {Response, SuperTest, Test} from 'supertest';
import app from '../src/app';
import {pErr} from '@shared';
import {OK} from 'http-status-codes';

describe('Data routes', () => {
    let agent: SuperTest<Test>;

    const datasPath = '/datas';

    beforeAll((done) => {
        agent = supertest.agent(app);
        done();
    });

    describe('upload csv file', () => {
        it('should return a 200', () => {
            agent
                .post(datasPath)
                .attach('file', './spec/resources/test.csv')
                .end((err: Error, res: Response) => {
                    pErr(err);
                    expect(res.status).toBe(OK);
                });
        });
    });
})
